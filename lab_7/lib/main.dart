import 'package:flutter/material.dart';
import 'package:lab_7/screens/starting_page.dart';
import 'screens/signup_page.dart';
import 'screens/login_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'DMSans',
        primaryColor: const Color.fromRGBO(101, 204, 184, 1),
        secondaryHeaderColor: const Color.fromRGBO(79, 232, 180, 1),
      ),
      title: 'Corum App',
      home: const StartingPage(),
      routes: {
        SignupPage.routeName: (ctx) => const SignupPage(),
        LoginPage.routeName: (ctx) => const LoginPage(),
      },
    );
  }
}
