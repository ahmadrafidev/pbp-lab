from django.db import models

# Create your models here.

class Note(models.Model):
  to = models.CharField(max_length=100)
  From =  models.CharField(max_length=100)
  title = models.CharField(max_length=100)
  message = models.TextField()

  def __str__(self):
      return self.title
