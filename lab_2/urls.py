from django.urls import path, re_path
from .views import index, xml_view, json_view

urlpatterns = [
    path('',index,name='index'),
    path('xml', xml_view, name='xml'),
    path('json', json_view, name='json'),
]

