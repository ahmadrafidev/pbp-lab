from lab_2.models import Note
from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.


def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml_view(request):
    xmlSet = Note.objects.all()
    data = serializers.serialize('xml', xmlSet)
    return HttpResponse(data, content_type="application/xml")

def json_view(request):
    jsonSet = Note.objects.all()
    data = serializers.serialize('json', jsonSet)
    return HttpResponse(data, content_type="application/json")
