from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=100)
    npm = models.BigIntegerField()
    DOB = models.DateField()
    # TODO Implement missing attributes in Friend model

    def __str__(self):
        return self.name