from django.urls import path, re_path
from .views import index, friend_list

urlpatterns = [
    path('', index, name='index'),
    path('friends', friend_list)
    # TODO Add friends path using friend_list Views
]
