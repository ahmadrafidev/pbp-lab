import 'package:flutter/material.dart';
import 'package:lab_6/screens/home_screen.dart';
import 'package:lab_6/screens/tabs_screen.dart';
import 'screens/starting_page.dart';
import 'screens/home_screen.dart';
import 'screens/events_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'DMSans',
        primaryColor: const Color.fromRGBO(101, 204, 184, 1),
        secondaryHeaderColor: const Color.fromRGBO(79, 232, 180, 1),
        ),
      title: 'Corum App',
      home: const StartingPage(),
      routes: {
        TabsScreen.routeName: (ctx) => const TabsScreen(),
        HomeScreen.routeName: (ctx) => const HomeScreen(),
        EventsScreen.routeName: (ctx) => const EventsScreen(),
      },
    );
  }
}
